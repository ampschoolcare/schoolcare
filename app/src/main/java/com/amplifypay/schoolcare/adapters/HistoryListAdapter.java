package com.amplifypay.schoolcare.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.activities.TransactionDetailActivity;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.methods.HistoryListItem;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.amplifypay.schoolcare.utils.AppUtil;

import java.util.List;

/**
 * Created by eit on 4/7/16.
 */
public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {
    private static LayoutInflater inflater = null;
    private Merchant merchant;
    Context mContext;
    private List<HistoryListItem> historyListItemList;
    APIService service;
    String formatted_amount;

    public HistoryListAdapter(Context context, List<HistoryListItem> objects){
        mContext = context;
        merchant = Merchant.getThisMerchant();
        service = RestClient.getClient();

        this.historyListItemList = objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.trans_history_list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HistoryListItem historyListItem = historyListItemList.get(position);

        formatted_amount = String.format("%,d", Integer.valueOf(historyListItem.getOrder_amount()));

        holder.trans_date.setText(AppUtil.dateReadable(mContext, historyListItem.getDate()));

        holder.trans_amount.setText("₦"+ formatted_amount);
        holder.trans_description.setText(historyListItem.getOrder_description());
        holder.order_status.setText(historyListItem.getOrder_status());
        holder.account_number.setText("Acct Number - " + historyListItem.getAccount_number());

        if (historyListItem.getOrder_status().equalsIgnoreCase("Approved")){
            holder.order_status.setTextColor(mContext.getResources().getColor( R.color.primaryColor));
        }else if (historyListItem.getOrder_status().equalsIgnoreCase("Declined")){
            holder.order_status.setTextColor(mContext.getResources().getColor( R.color.error_color));
        }else {
            holder.order_status.setTextColor(mContext.getResources().getColor( R.color.textColor));
        }

    }


    @Override
    public int getItemCount() {
        return historyListItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView trans_date;
        public TextView trans_description;
        public TextView trans_amount;
        public TextView order_status;
        public TextView account_number;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            trans_date = (TextView) itemView.findViewById(R.id.trans_date);
            trans_description = (TextView) itemView.findViewById(R.id.trans_desc);
            trans_amount = (TextView) itemView.findViewById(R.id.trans_amount);
            order_status = (TextView) itemView.findViewById(R.id.order_status);
            account_number = (TextView) itemView.findViewById(R.id.trans_acct_number);


        }

        @Override
        public void onClick(View v) {
            final HistoryListItem historyListItem = historyListItemList.get(getPosition());
            Intent i = new Intent(mContext, TransactionDetailActivity.class);
            i.putExtra("school_id", historyListItem.getSchool_id());
            i.putExtra("transaction_id", historyListItem.getTransaction_id());
            i.putExtra("student_name", historyListItem.getStudent_name());
            i.putExtra("student_gender", historyListItem.getStudent_gender());
            i.putExtra("order_amount", historyListItem.getOrder_amount());
            i.putExtra("order_description", historyListItem.getOrder_description());
            i.putExtra("order_date", historyListItem.getDate());
            i.putExtra("order_status", historyListItem.getOrder_status());
            i.putExtra("account_number", historyListItem.getAccount_number());
            i.putExtra("formatted_amount", formatted_amount);



            mContext.startActivity(i);

        }
    }
}
