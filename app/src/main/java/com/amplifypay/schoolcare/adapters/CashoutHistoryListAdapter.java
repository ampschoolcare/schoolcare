package com.amplifypay.schoolcare.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.activities.CashoutDetailActivity;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.methods.CashoutListItem;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;

import java.util.List;

/**
 * Created by eit on 4/7/16.
 */
public class CashoutHistoryListAdapter extends RecyclerView.Adapter<CashoutHistoryListAdapter.ViewHolder> {
    private static LayoutInflater inflater = null;
    private Merchant merchant;
    Context mContext;
    private List<CashoutListItem> historyListItemList;
    APIService service;
    String formatted_amount;

    public CashoutHistoryListAdapter(Context context, List<CashoutListItem> objects){
        mContext = context;
        merchant = Merchant.getThisMerchant();
        service = RestClient.getClient();

        this.historyListItemList = objects;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.cashout_history_list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CashoutListItem historyListItem = historyListItemList.get(position);

        formatted_amount = String.format("%,d", Integer.valueOf(historyListItem.getCashout_amount()));

        holder.request_date.setText(historyListItem.getRequest_date());

        holder.amount.setText("₦"+ formatted_amount);
        holder.request_status.setText(historyListItem.getRequest_status());
        holder.handled_by.setText(historyListItem.getRequest_handled_by());
        holder.disbursed_date.setText(historyListItem.getRequest_disburse_date());

        if (historyListItem.getRequest_status().equalsIgnoreCase("Approved")){
            holder.request_status.setTextColor(mContext.getResources().getColor( R.color.primaryColor));
        }else if (historyListItem.getRequest_status().equalsIgnoreCase("Pending")){
            holder.request_status.setTextColor(mContext.getResources().getColor( R.color.error_color));
        }else {
            holder.request_status.setTextColor(mContext.getResources().getColor(R.color.textColor));
        }

    }


    @Override
    public int getItemCount() {
        return historyListItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView request_date;
        public TextView amount;
        public TextView request_status;
        public TextView handled_by;
        public TextView disbursed_date;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            request_date = (TextView) itemView.findViewById(R.id.trans_date);
            amount = (TextView) itemView.findViewById(R.id.cashout_amount_text);
            request_status = (TextView) itemView.findViewById(R.id.order_status);
            handled_by = (TextView) itemView.findViewById(R.id.request_handled_by);
            disbursed_date = (TextView) itemView.findViewById(R.id.disburse_date_text);


        }

        @Override
        public void onClick(View v) {
            final CashoutListItem historyListItem = historyListItemList.get(getPosition());
            Intent i = new Intent(mContext, CashoutDetailActivity.class);
            i.putExtra("request_id", historyListItem.getRequest_id());
            i.putExtra("request_date", historyListItem.getRequest_date());
            i.putExtra("formatted_amount", formatted_amount);
            i.putExtra("cashout_amount", historyListItem.getCashout_amount());
            i.putExtra("additional_info", historyListItem.getAdditional_info());
            i.putExtra("request_status", historyListItem.getRequest_status());
            i.putExtra("request_disburse_date", historyListItem.getRequest_disburse_date());
            i.putExtra("request_handled_by", historyListItem.getRequest_handled_by());
            i.putExtra("request_date_handled", historyListItem.getRequest_date_handled());

            mContext.startActivity(i);

        }
    }
}
