package com.amplifypay.schoolcare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.methods.AdapterItem;

import java.util.List;

/**
 * Created by eit on 7/24/16.
 */
public class SchoolItemAdapter extends ArrayAdapter<AdapterItem> {
    private static LayoutInflater inflater=null;
    private List<AdapterItem> schoolItemList;


    public SchoolItemAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public SchoolItemAdapter(Context context, int resource, List<AdapterItem> items) {
        super(context, resource, items);
        this.schoolItemList = items;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        final ViewHolder holder;
        final AdapterItem schoolItem = schoolItemList.get(position);

        if(convertView==null){
            view = inflater.inflate(R.layout.school_list_item, null);
            holder = new ViewHolder();
            holder.textViewName = (TextView) view.findViewById(R.id.textViewName);
            holder.textViewId=(TextView)view.findViewById(R.id.textViewId);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }
        holder.textViewId.setText(schoolItem.getId());
        holder.textViewName.setText(schoolItem.getName());

        return view;
    }

    public int getItemIndexByName(String name) {
        for (AdapterItem item : this.schoolItemList) {
            if(item.getName().toString().equals(name.toString())){
                return this.schoolItemList.indexOf(item) + 1;
            }
        }
        return 0;
    }

    public int getItemIndexById(String id) {
        for (AdapterItem item : this.schoolItemList) {
            if(item.getId().toString().equals(id.toString())){
                return this.schoolItemList.indexOf(item) + 1;
            }
        }
        return 0;
    }

    public static class ViewHolder{
        public TextView textViewName;
        public TextView textViewId;

    }
}
