package com.amplifypay.schoolcare.app.model;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class School {
    public static final String TAG = "School";

    @DatabaseField(id = true)
    public long uniqueid;

    @DatabaseField
    public String school_name;

    @DatabaseField
    public String school_short_name;

    // Default constructor is needed for the SQLite, so make sure you also have it
    public School(){

    }

    public School(long uniqueid, String school_name, String school_short_name){
        this.uniqueid = uniqueid;
        this.school_name = school_name;
        this.school_short_name = school_short_name;

    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getSchoolDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }
}
