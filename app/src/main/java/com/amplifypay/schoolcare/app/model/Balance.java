package com.amplifypay.schoolcare.app.model;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class Balance {
    public static final String TAG = "Balance";

//    @DatabaseField(id = true)
//    public int id;

    @DatabaseField
    public String student_name;

    @DatabaseField
    public String student_gender;

    @DatabaseField
    public String parent_phone;

    @DatabaseField
    public String account_number;

    @DatabaseField
    public String account_balance;

    @DatabaseField
    public String school_id;


    // Default constructor is needed for the SQLite, so make sure you also have it
    public Balance(){

    }

    public Balance(String student_name, String student_gender, String parent_phone, String account_number, String account_balance, String school_id){
        this.student_name = student_name;
        this.student_gender = student_gender;
        this.parent_phone = parent_phone;
        this.account_number = account_number;
        this.account_balance = account_balance;
        this.school_id = school_id;
    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getBalanceDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }
}
