package com.amplifypay.schoolcare.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.amplifypay.schoolcare.app.model.School;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.responses.SchoolsResponse;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eit on 7/24/16.
 */
public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();
    public static String androidClientUsername = "platformuser";
    public static String androidClientPassword = "platformadmin";
    APIService service;
    Context context;

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

        context = getApplicationContext();
        service = RestClient.getClient();

        getSchools();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public static DatabaseHelper getDatabaseHelper(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        databaseHelper.getWritableDatabase();
        return databaseHelper;
    }

    private void getSchools() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("client_user", AppController.androidClientUsername);
        jsonObject.addProperty("client_pass", AppController.androidClientPassword);

        Call<SchoolsResponse> call = service.getSchools(jsonObject);
        call.enqueue(new Callback<SchoolsResponse>() {
            @Override
            public void onResponse(Call<SchoolsResponse> call, Response<SchoolsResponse> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    SchoolsResponse schoolsResponse = response.body();
                    List<School> schools = schoolsResponse.getSchools();
                    int size = schools.size();
                    Log.e("school size", size + " ");
                    if (size > 0) {
                        for (School school : schools) {
                            school = new School(school.uniqueid, school.school_name, school.school_short_name);
                            school.save();
                        }
                    }
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<SchoolsResponse> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });
    }
}
