package com.amplifypay.schoolcare.app.model;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class Cashout {
    public static final String TAG = "Cashout";

    @DatabaseField(id = true)
    public int id;

    @DatabaseField
    public String uniqueid;

    @DatabaseField
    public String merchant_uniqueid;

    @DatabaseField
    public String school_id;

    @DatabaseField
    public String amount;

    @DatabaseField
    public String additional_info;

    @DatabaseField
    public String request_date;

    @DatabaseField
    public String request_disburse_date;

    @DatabaseField
    public String request_status;

    @DatabaseField
    public String request_handled_by;

    @DatabaseField
    public String request_date_handled;

    @DatabaseField
    public String datecreated;

    @DatabaseField
    public String datemodified;


    // Default constructor is needed for the SQLite, so make sure you also have it
    public Cashout(){

    }

    public Cashout(int id, String uniqueid, String school_id, String merchant_uniqueid, String amount, String additional_info, String request_date, String request_disburse_date, String request_status, String request_handled_by, String request_date_handled, String datecreated, String datemodified){
        this.id = id;
        this.uniqueid = uniqueid;
        this.school_id = school_id;
        this.merchant_uniqueid = merchant_uniqueid;
        this.amount = amount;
        this.additional_info = additional_info;
        this.request_date = request_date;
        this.request_disburse_date = request_disburse_date;
        this.request_status = request_status;
        this.request_handled_by = request_handled_by;
        this.request_date_handled = request_date_handled;
        this.datecreated = datecreated;
        this.datemodified = datemodified;

    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getCashoutDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }
}
