package com.amplifypay.schoolcare.app.model;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class Merchant {
    public static final String TAG = "Merchant";

    @DatabaseField(id = true)
    public String id;

    @DatabaseField
    public String uniqueid;

    @DatabaseField
    public String username;

    @DatabaseField
    public String password;

    @DatabaseField
    public String fullname;

    @DatabaseField
    public String email_address;

    @DatabaseField
    public String phone_number;

    @DatabaseField
    public String school_id;

    @DatabaseField
    public String privileges;


    @DatabaseField
    public String status;

    @DatabaseField
    public String datecreated;

    @DatabaseField
    public String datemodified;

    // Default constructor is needed for the SQLite, so make sure you also have it
    public Merchant(){

    }

    public Merchant(String  id, String uniqueid, String username, String password, String fullname, String email_address, String phone_number, String school_id, String privileges, String status, String datecreated, String datemodified){
        this.id = id;
        this.uniqueid = uniqueid;
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email_address = email_address;
        this.phone_number = phone_number;
        this.school_id = school_id;
        this.privileges = privileges;
        this.status = status;
        this.datecreated = datecreated;
        this.datemodified = datemodified;
    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getMerchantDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }

    public static Merchant getThisMerchant(){
        try {
            if (AppController.getDatabaseHelper(AppController.getInstance()).getMerchantDao().queryForAll().size() != 0) {
                try {
                    return AppController.getDatabaseHelper(AppController.getInstance()).getMerchantDao().queryForAll().get(0);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
