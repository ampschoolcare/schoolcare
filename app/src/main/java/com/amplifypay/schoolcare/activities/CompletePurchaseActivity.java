package com.amplifypay.schoolcare.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.rest.responses.OrderResponse;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.amplifypay.schoolcare.utils.AppUtil;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompletePurchaseActivity extends AppCompatActivity {
    Intent intent;
    String agent_id, school_id, transaction_id, student_name, student_gender, order_amount, formatted_amount, order_description, account_number;
    TextView student_name_text, student_gender_text, order_amount_text, order_description_text, card_number_text, pay_button;
    APIService service;
    Context context;

    ImageView backArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_purchase);

        service = RestClient.getClient();
        this.context = getApplicationContext();


        intent = getIntent();
        agent_id = intent.getStringExtra("agent_id");
        school_id = intent.getStringExtra("school_id");
        transaction_id = intent.getStringExtra("transaction_id");
        student_name = intent.getStringExtra("student_name");
        student_gender = intent.getStringExtra("student_gender");
        order_amount = intent.getStringExtra("order_amount");
        formatted_amount = intent.getStringExtra("formatted_amount");
        account_number = intent.getStringExtra("account_number");
        order_description = intent.getStringExtra("order_description");

        setUpViews();


    }

    private void setUpViews(){
        backArrow =(ImageView)findViewById(R.id.back_arrow);

        student_name_text = (TextView) findViewById(R.id.student_name);
        student_name_text.setText(student_name);

        student_gender_text = (TextView) findViewById(R.id.student_gender);
        student_gender_text.setText(student_gender);

        order_amount_text = (TextView) findViewById(R.id.amount_text);
        order_amount_text.setText("₦"+ formatted_amount);

        order_description_text = (TextView) findViewById(R.id.description);
        order_description_text.setText(order_description);

        card_number_text = (TextView) findViewById(R.id.card_number_text);
        card_number_text.setText("... " + account_number.substring(account_number.length() - 4));

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        pay_button = (TextView) findViewById(R.id.pay_button);

        pay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePurchase();
            }
        });



    }

    private void makePurchase(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("agent_id", agent_id);
        jsonObject.addProperty("school_id", school_id);
        jsonObject.addProperty("transaction_id", transaction_id);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Confirming...");
        pDialog.show();


        Call<OrderResponse> call = service.makePurchase(jsonObject);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    OrderResponse orderResponse = response.body();
                    orderResponse.save();
                    alertPaymentCompleteDialog(orderResponse.order_status, String.valueOf(orderResponse.id), orderResponse.response_description);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });

    }

    private void alertPaymentCompleteDialog(String order_status, String transaction_id, String response_description){
        if (order_status.equalsIgnoreCase("APPROVED")){
            AppUtil.showTransactionResponse(CompletePurchaseActivity.this, true, "Tran ID: #" + transaction_id + " - " + order_status , new AppUtil.Callback() {
                @Override
                public void callback() {
                    onBackPressed();
                }
            });
        }else if (order_status.equalsIgnoreCase("DECLINED")){
            AppUtil.showTransactionResponse(CompletePurchaseActivity.this, false, response_description, new AppUtil.Callback() {
                @Override
                public void callback() {
                    onBackPressed();
                }
            });
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



}
