package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.model.Cashout;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashoutDetailActivity extends AppCompatActivity {
    Intent intent;
    String request_id, request_date, formatted_amount, cashout_amount, additional_info, request_status, request_disburse_date, request_handled_by, request_date_handled;
    TextView request_date_text, cashout_amount_text, additional_info_text, request_status_text, request_disburse_date_text, request_handled_by_text, request_date_handled_text;
    APIService service;
    Context context;
    ImageView backArrow;
    private AppCompatButton btnCancelCashout;
    Merchant merchant;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashout_detail);

        this.context = getApplicationContext();
        service = RestClient.getClient();

        merchant = Merchant.getThisMerchant();

        intent = getIntent();
        request_id = intent.getStringExtra("request_id");
        request_date = intent.getStringExtra("request_date");
        cashout_amount = intent.getStringExtra("cashout_amount");
        additional_info = intent.getStringExtra("additional_info");
        request_status = intent.getStringExtra("request_status");
        request_disburse_date = intent.getStringExtra("request_disburse_date");
        request_handled_by = intent.getStringExtra("request_handled_by");
        request_date_handled = intent.getStringExtra("request_date_handled");
        formatted_amount = intent.getStringExtra("formatted_amount");


        setUpViews();


    }

    private void setUpViews(){
        backArrow =(ImageView)findViewById(R.id.back_arrow);
        btnCancelCashout = (AppCompatButton) findViewById(R.id.btn_cancel_cashout);

        request_date_text = (TextView) findViewById(R.id.request_date_text);
        request_date_text.setText(request_date);

        request_disburse_date_text = (TextView) findViewById(R.id.disburse_date_text);
        request_disburse_date_text.setText(request_disburse_date);

        request_date_handled_text = (TextView) findViewById(R.id.date_handled);
        request_date_handled_text.setText(request_date_handled);

        cashout_amount_text = (TextView) findViewById(R.id.amount_text);
        cashout_amount_text.setText("₦"+ formatted_amount);

        additional_info_text = (TextView) findViewById(R.id.description);
        additional_info_text.setText(additional_info);

        request_handled_by_text = (TextView) findViewById(R.id.request_handled_by);
        request_handled_by_text.setText(request_handled_by);

        request_status_text = (TextView) findViewById(R.id.cashout_status);
        request_status_text.setText(request_status);

        if (request_status.equalsIgnoreCase("Approved")){
            request_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }else if (request_status.equalsIgnoreCase("Pending")){
            request_status_text.setTextColor(getResources().getColor( R.color.error_color));
        }else {
            request_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }


        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnCancelCashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeCancelCashoutRequest(request_id);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void makeCancelCashoutRequest(String request_id){

        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("request_id", request_id);
        }

        Call<Cashout> call = service.canceCashoutRequest(jsonObject);
        call.enqueue(new Callback<Cashout>() {
            @Override
            public void onResponse(Call<Cashout> call, Response<Cashout> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Cashout cashout = response.body();
                    cashout.save();
                    Toast.makeText(context, "Cashout request cancelled", Toast.LENGTH_SHORT).show();
                    hideCancelButton();
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<Cashout> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });

    }

    private void hideCancelButton() {
        btnCancelCashout.setVisibility(View.GONE);
    }



}
