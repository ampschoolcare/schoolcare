package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.amplifypay.schoolcare.utils.AppUtil;

public class ApprovedDetailActivity extends AppCompatActivity {
    Intent intent;
    String order_date, school_id, transaction_id, formatted_amount, student_name, student_gender, order_amount, account_number, order_description, order_status;
    TextView student_name_text, student_gender_text, acct_number_text, order_amount_text, order_description_text, transaction_id_text, order_date_text, order_status_text, school_id_text;
    APIService service;
    Context context;
    private AppCompatButton requestRefundBtn;


    ImageView backArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approved_detail);

        service = RestClient.getClient();
        this.context = getApplicationContext();


        intent = getIntent();
        transaction_id = intent.getStringExtra("transaction_id");
        school_id = intent.getStringExtra("school_id");
        order_date = intent.getStringExtra("order_date");
        student_name = intent.getStringExtra("student_name");
        student_gender = intent.getStringExtra("student_gender");
        order_amount = intent.getStringExtra("order_amount");
        order_description = intent.getStringExtra("order_description");
        order_status = intent.getStringExtra("order_status");
        account_number = intent.getStringExtra("account_number");
        formatted_amount = intent.getStringExtra("formatted_amount");



        setUpViews();


    }

    private void setUpViews(){
        backArrow =(ImageView)findViewById(R.id.back_arrow);

        requestRefundBtn =(AppCompatButton)findViewById(R.id.btn_refund_request);

        order_date_text = (TextView) findViewById(R.id.date_text);
        order_date_text.setText(AppUtil.dateReadable(this, order_date));

        student_name_text = (TextView) findViewById(R.id.student_name);
        student_name_text.setText(student_name);

        student_gender_text = (TextView) findViewById(R.id.student_gender);
        student_gender_text.setText(student_gender);

        acct_number_text = (TextView) findViewById(R.id.student_account_number);
        acct_number_text.setText(account_number);

        order_amount_text = (TextView) findViewById(R.id.amount_text);
        order_amount_text.setText("₦"+ formatted_amount);

        order_description_text = (TextView) findViewById(R.id.description);
        order_description_text.setText(order_description);

        transaction_id_text = (TextView) findViewById(R.id.trans_id);
        transaction_id_text.setText(transaction_id);

        order_status_text = (TextView) findViewById(R.id.order_status);
        order_status_text.setText(order_status);

        if (order_status.equalsIgnoreCase("Approved")){
            order_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }else if (order_status.equalsIgnoreCase("Declined")){
            order_status_text.setTextColor(getResources().getColor( R.color.error_color));
        }else {
            order_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }

        school_id_text = (TextView) findViewById(R.id.sch_id);
        school_id_text.setText(school_id);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        requestRefundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRefundFragment();
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void goToRefundFragment() {
        Intent i = new Intent(ApprovedDetailActivity.this, MainActivity.class);
        i.putExtra("transaction_id", transaction_id);
        startActivity(i);
    }


}
