package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.adapters.CashoutHistoryListAdapter;
import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.model.Cashout;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.methods.CashoutListItem;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.responses.CashoutRequestsResponse;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashoutHistoryActivity extends AppCompatActivity {

    APIService service;
    Merchant merchant;
    RecyclerView transHistoryRecyclerView;
    CashoutHistoryListAdapter historyListAdapter;
    private List<CashoutListItem> historyListItemList = new ArrayList<>();
    TextView merchantDataView;
    Context context;
    Boolean isConnected;
    ImageView backArrow;

    private int pageNumber = 1;
    private int visibleThreshold = 1;
    private int previousTotal = 0;
    private boolean loading = true;
    private int lastVisibleItem, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashout_history);
        this.context = getApplicationContext();
        merchant = Merchant.getThisMerchant();
        service = RestClient.getClient();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews();

        getCashoutHistory();
    }
    private void getCashoutHistory(){
        getCashoutFromDB();
        getCashoutHistoryFromServer(1);
    }

    private void setUpViews(){
        backArrow =(ImageView)findViewById(R.id.back_arrow);
        merchantDataView =(TextView)findViewById(R.id.merchant_details);
        if (merchant != null){
            merchantDataView.setText(merchant.fullname);
        }
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        transHistoryRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        historyListAdapter = new CashoutHistoryListAdapter(CashoutHistoryActivity.this, historyListItemList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CashoutHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
        transHistoryRecyclerView.setLayoutManager(linearLayoutManager);

        transHistoryRecyclerView.setAdapter(historyListAdapter);


        transHistoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        pageNumber++;
                    }
                }


                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    findViewById(R.id.loadMore).setVisibility(View.VISIBLE);
                    getCashoutHistoryFromServer(pageNumber);
                    loading = true;
                }
            }
        });
    }

    private void getCashoutHistoryFromServer(final int pageNumber) {
        final ProgressBar loadingPanel = (ProgressBar) findViewById(R.id.loadingPanel);
        final TextView noTransactionsText = (TextView) findViewById(R.id.no_like_text);

        if (isConnected) {
            if (merchant != null) {
                Call<CashoutRequestsResponse> call = service.getCashOutHistory(merchant.uniqueid, merchant.school_id, String.valueOf(pageNumber));
                call.enqueue(new Callback<CashoutRequestsResponse>() {
                    @Override
                    public void onResponse(Call<CashoutRequestsResponse> call, Response<CashoutRequestsResponse> response) {
                        if (response.isSuccessful()) {
                            loadingPanel.setVisibility(View.GONE);
                            findViewById(R.id.loadMore).setVisibility(View.GONE);


                            // request successful (status code 200, 201)
                            CashoutRequestsResponse transactionsResponse = response.body();

                            List<Cashout> cashoutResponses = transactionsResponse.getCashout_requests();
                            int size = cashoutResponses.size();
                            if (size == 0 && pageNumber == 1) {
                                noTransactionsText.setVisibility(View.VISIBLE);
                            }
                            else {
                                noTransactionsText.setVisibility(View.GONE);
                                for (Cashout cashout : cashoutResponses) {
                                    if (pageNumber == 1){
                                        cashout.save();
                                    }
                                    historyListItemList.add(new CashoutListItem(String.valueOf(cashout.id), cashout.request_date, cashout.amount, cashout.additional_info, cashout.request_status, cashout.request_disburse_date, cashout.request_handled_by, cashout.request_date_handled));
                                }
                            }
                            historyListAdapter.notifyDataSetChanged();

                        } else {
                            //request not successful (like 400,401,403 etc)
                            //Handle errors
                            APIError error = LoggingInterceptor.getLastApiError();
                            if (error != null) {
                                // … log the issue :)
                                Log.e("error message", error.msg());
                                // … and use it to show error information
                                Toast.makeText(CashoutHistoryActivity.this, error.msg(), Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<CashoutRequestsResponse> call, Throwable t) {
                        Log.e("response", "failed");
                        Log.e("response", t + "");

                    }
                });
            }
        } else {
            Toast.makeText(this.context, "No internet Connection, Try again", Toast.LENGTH_LONG).show();
            loadingPanel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getCashoutFromDB() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        try {
            Dao<Cashout, Integer> cashoutIntegerDao = AppController.getDatabaseHelper(AppController.getInstance()).getCashoutDao();

            QueryBuilder<Cashout, Integer> cashoutIntegerQueryBuilder = cashoutIntegerDao.queryBuilder().orderBy("id", false);
            List<Cashout> cashoutList = cashoutIntegerQueryBuilder.query();
            if (!cashoutList.isEmpty()) {
                for (Cashout cashout : cashoutList) {
                    findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    historyListItemList.add(new CashoutListItem(String.valueOf(cashout.id), cashout.request_date, cashout.amount, cashout.additional_info, cashout.request_status, cashout.request_disburse_date, cashout.request_handled_by, cashout.request_date_handled));
                }

                historyListAdapter.notifyDataSetChanged();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
