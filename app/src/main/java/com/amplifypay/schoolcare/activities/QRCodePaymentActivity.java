package com.amplifypay.schoolcare.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.responses.OrderResponse;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.JsonObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QRCodePaymentActivity extends AppCompatActivity {

    SurfaceView cameraView;
    TextView barcodeInfo;
    BarcodeDetector barcodeDetector;

    Button cancelButton, proceedButton;
    CameraSource cameraSource;
    String card_number, amount, description, formatted_amount;
    APIService service;
    Context context;
    Merchant merchant;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_payment);

        service = RestClient.getClient();
        this.context = getApplicationContext();
        merchant = Merchant.getThisMerchant();

        intent = getIntent();
        amount = intent.getStringExtra("amount");
        formatted_amount = intent.getStringExtra("formatted_amount");
        description = intent.getStringExtra("description");

        cameraView = (SurfaceView)findViewById(R.id.camera_view);
        barcodeInfo = (TextView)findViewById(R.id.code_info);

        cancelButton = (Button)findViewById(R.id.btn_cancel);
        proceedButton = (Button)findViewById(R.id.btn_proceed);

        barcodeDetector = new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.QR_CODE)
                        .build();

        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(320, 400)
                .build();


        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    barcodeInfo.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            barcodeInfo.setText(    // Update the TextView
                                    barcodes.valueAt(0).displayValue
                            );
                            if (!barcodes.valueAt(0).displayValue.isEmpty()){
                                validateQRCodeCardNumber();
                                showActionButtons();
                            }
                        }
                    });
                }
            }
        });
    }

    private void showActionButtons(){
        cancelButton.setVisibility(View.VISIBLE);
        proceedButton.setVisibility(View.VISIBLE);


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitData();
            }
        });
    }

    private void submitData(){
        //validate then submit
        if (!validateQRCodeCardNumber()) {
            return;
        }
        //send to confirm order endpoint
        confirmOrder();

    }

    private void confirmOrder() {
        card_number = barcodeInfo.getText().toString().trim();
        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("account_number", card_number);
            jsonObject.addProperty("amount", amount);
            jsonObject.addProperty("order_description", description);
        }
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Initializing payment...");
        pDialog.show();

        Call<OrderResponse> call = service.confirmOrder(jsonObject);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    OrderResponse orderResponse = response.body();
                    orderResponse.save();
                    completePurchase(orderResponse);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });
    }

    private void completePurchase(OrderResponse orderResponse){
        Intent i = new Intent(context, CompletePurchaseActivity.class);
        i.putExtra("agent_id", orderResponse.agent_id);
        i.putExtra("school_id", orderResponse.school_id);
        i.putExtra("transaction_id", String.valueOf(orderResponse.id));
        i.putExtra("student_name", orderResponse.student_name);
        i.putExtra("student_gender", orderResponse.student_gender);
        i.putExtra("order_amount", orderResponse.amount);
        i.putExtra("formatted_amount", formatted_amount);
        i.putExtra("account_number", orderResponse.account_number);
        i.putExtra("order_description", orderResponse.order_description);

        Log.e("account_number", orderResponse.account_number);


        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean validateQRCodeCardNumber() {
        if (barcodeInfo.getText().toString().trim().isEmpty()) {
            barcodeInfo.setError("Scan again, your QR Code must return a 10 digits number");
            return false;
        } else if (barcodeInfo.getText().toString().trim().length() < 10){
            barcodeInfo.setError("Scan again, your QR Code number cannot be less than 10 digits");
            return false;
        } else if (barcodeInfo.getText().toString().trim().length() > 10){
            barcodeInfo.setError("Scan again, your QR Code number cannot be more than 10 digits");
            return false;
        }


        return true;
    }


}
