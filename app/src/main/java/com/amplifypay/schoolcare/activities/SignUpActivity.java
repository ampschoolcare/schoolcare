package com.amplifypay.schoolcare.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.adapters.SchoolItemAdapter;
import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.app.model.School;
import com.amplifypay.schoolcare.methods.AdapterItem;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.amplifypay.schoolcare.services.SchoolService;
import com.google.gson.JsonObject;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

//    private Toolbar toolbar;
    private EditText inputMerchantId;
    private TextInputLayout inputLayoutMechantId;
    private MaterialSpinner inputSchoolSpinner;
    private AppCompatButton btnSignUp;

    private List<AdapterItem> school_ids = new ArrayList<>();
    private SchoolItemAdapter mSchoolItemAdapter;
    private String schoolId, merchantId;
    Context context;
    APIService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        this.context = getApplicationContext();
        service = RestClient.getClient();
        context.startService(new Intent(context, SchoolService.class));
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        if (isConnected){
            Log.e("network", isConnected + "");
        }else {
            Log.e("network", isConnected + "");
            Toast.makeText(context, "No internet access - Try again", Toast.LENGTH_SHORT).show();
        }
        setUpViews();
    }

    private void setUpViews(){
        inputLayoutMechantId = (TextInputLayout) findViewById(R.id.merchantIdWrapper);

        inputLayoutMechantId.setHint("Enter Merchant ID");
        inputMerchantId = (EditText) findViewById(R.id.mechant_id);
        btnSignUp = (AppCompatButton) findViewById(R.id.btn_start);

        mSchoolItemAdapter = new SchoolItemAdapter(this, R.layout.school_list_item, school_ids);
        mSchoolItemAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        inputSchoolSpinner = (MaterialSpinner) findViewById(R.id.school_spinner);
        inputSchoolSpinner.setAdapter(mSchoolItemAdapter);
        getSchoolData();

        mSchoolItemAdapter.notifyDataSetChanged();

        inputSchoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position < 0) {
                    return; //do nothing
                } else {
                    //do the rest of your code now
                    if (school_ids.isEmpty()) {
                        return;
                    } else {
//                TODO: catch if selected index less than zero
                        schoolId = school_ids.get(position).getId();
//                        Log.e("hair_type id", hairTypeId);
                    }
                    if (schoolId.isEmpty()) {
                        Toast.makeText(SignUpActivity.this, "Please select your school", Toast.LENGTH_LONG).show();
                        inputSchoolSpinner.setError("Please select your school");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(SignUpActivity.this, "Please select your school", Toast.LENGTH_LONG).show();
                inputSchoolSpinner.setError("Please select your school");

            }
        });

        inputMerchantId.addTextChangedListener(new MyTextWatcher(inputMerchantId));
//        merchantId = inputMerchantId.getText().toString().trim();

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm(schoolId);
            }
        });
    }

    /**
     * Validating form
     */
    private void submitForm(String schoolId) {
        merchantId = inputMerchantId.getText().toString().trim();
        if (!validateMerchantId()) {
            return;
        }

        if (!validateSchoolSpinner(schoolId)) {
            return;
        }

        // send data to server - in the absence of api endpoints start
        authMerchant(merchantId, schoolId);
    }

    private boolean validateMerchantId() {
        if (inputMerchantId.getText().toString().trim().isEmpty()) {
            inputLayoutMechantId.setError("Please enter Your Merchant ID");
            requestFocus(inputMerchantId);
            return false;
        } else {
            inputLayoutMechantId.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSchoolSpinner(String schoolId) {
        if (schoolId == null) {
            Toast.makeText(getApplicationContext(), "Please select your school", Toast.LENGTH_LONG).show();
            inputSchoolSpinner.setError("Please select your school");
            requestFocus(inputSchoolSpinner);
            return false;
        } else {
//            inputSchoolSpinner.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.mechant_id:
                    validateMerchantId();
                    break;
            }
        }
    }

    private void getSchoolData(){
        try {
            Dao<School,Integer> schoolIntegerDao = AppController.getDatabaseHelper(AppController.getInstance()).getSchoolDao();
            QueryBuilder<School,Integer> schoolIntegerQueryBuilder = schoolIntegerDao.queryBuilder();
            schoolIntegerQueryBuilder.orderBy("uniqueid",false);
            List<School> schoolList =schoolIntegerQueryBuilder.query();
            for (School school: schoolList){
                school_ids.add(new AdapterItem(String.valueOf(school.uniqueid), school.school_short_name));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void startTransaction(){
        Intent i = new Intent(SignUpActivity.this, MainActivity.class);
        startActivity(i);
    }

    private void authMerchant(String merchantId, String schoolId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("agent_id", merchantId);
        jsonObject.addProperty("school_id", schoolId);

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Authorizing...");
        pDialog.show();


        Call<Merchant> call = service.signInMerchant(jsonObject);
        call.enqueue(new Callback<Merchant>() {
            @Override
            public void onResponse(Call<Merchant> call, Response<Merchant> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Merchant merchant = response.body();
//                    Log.e("merchant response", merchant + "");
                    merchant.save();
                    startTransaction();

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }
            @Override
            public void onFailure(Call<Merchant> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });
    }


}
