package com.amplifypay.schoolcare.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.responses.OrderResponse;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardPaymentActivity extends AppCompatActivity {

    private Intent intent;

    private String amount, description, card_number, formatted_amount;
    APIService service;
    Merchant merchant;

    EditText cardField;
    TextView pay_top, pay_bottom;
    Context context;

    ImageView backArrow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        service = RestClient.getClient();
        this.context = getApplicationContext();
        merchant = Merchant.getThisMerchant();

        intent = getIntent();
        formatted_amount = intent.getStringExtra("formatted_amount");
        amount = intent.getStringExtra("amount");
        description = intent.getStringExtra("description");

        setUpViews();
    }

    private void setUpViews(){
        cardField = (EditText)findViewById(R.id.card_number);
        backArrow =(ImageView)findViewById(R.id.back_arrow);


        pay_bottom =(TextView)findViewById(R.id.pay_button);
        pay_top =(TextView)findViewById(R.id.pay1);
        pay_bottom.setText("Pay ₦"+ formatted_amount);
        pay_top.setText("Pay ₦"+ formatted_amount);

        cardField.addTextChangedListener(new MyTextWatcher(cardField));

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        pay_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        pay_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });


    }

    private void submitForm(){
        //validate then submit
        if (!validateCardNumber()) {
            return;
        }
        //send to confirm order endpoint
        confirmOrder();

    }

    private boolean validateCardNumber() {
        if (cardField.getText().toString().trim().isEmpty()) {
            cardField.setError("Please enter your 10 digits card number");
            requestFocus(cardField);
            return false;
        } else if (cardField.getText().toString().trim().length() < 10){
            cardField.setError("Your card number cannot be less than 10 digits");
            requestFocus(cardField);
            return false;
        } else if (cardField.getText().toString().trim().length() > 10){
            cardField.setError("Your card number cannot be more than 10 digits");
            requestFocus(cardField);
            return false;
        }


        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.card_number:
                    validateCardNumber();
                    break;
            }
        }
    }

    private void confirmOrder() {
        card_number = cardField.getText().toString().trim();
        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("account_number", card_number);
            jsonObject.addProperty("amount", amount);
            jsonObject.addProperty("order_description", description);
        }
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Payment in progress...");
        pDialog.show();

        Call<OrderResponse> call = service.confirmOrder(jsonObject);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    OrderResponse orderResponse = response.body();
                    orderResponse.save();
                    completePurchase(orderResponse);

                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });
    }

    private void completePurchase(OrderResponse orderResponse){
        Intent i = new Intent(context, CompletePurchaseActivity.class);
        Log.e("account_number", orderResponse.account_number);
        i.putExtra("agent_id", orderResponse.agent_id);
        i.putExtra("school_id", orderResponse.school_id);
        i.putExtra("transaction_id", String.valueOf(orderResponse.id));
        i.putExtra("student_name", orderResponse.student_name);
        i.putExtra("student_gender", orderResponse.student_gender);
        i.putExtra("order_amount", orderResponse.amount);
        i.putExtra("formatted_amount", formatted_amount);
        i.putExtra("account_number", orderResponse.account_number);
        i.putExtra("order_description", orderResponse.order_description);

        Log.e("transaction", orderResponse.id + "");

        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
