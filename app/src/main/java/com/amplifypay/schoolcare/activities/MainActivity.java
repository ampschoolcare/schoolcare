package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.fragments.CashoutFragment;
import com.amplifypay.schoolcare.fragments.CheckBalanceFragment;
import com.amplifypay.schoolcare.fragments.FilterFragment;
import com.amplifypay.schoolcare.fragments.PurchaseFragment;
import com.amplifypay.schoolcare.fragments.RefundFragment;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private int mNavItemId;
    Context context;
    private TextView actionbartitle, school_name_text;
    Merchant merchant;
    Intent intent;
    private String transaction_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);


        this.context = getApplicationContext();
        intent = getIntent();
        transaction_id = intent.getStringExtra("transaction_id");

        merchant = Merchant.getThisMerchant();
        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();

        // Lookup navigation view
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        // Setup drawer view
        setupDrawerContent(nvDrawer);

        // Tie DrawerLayout events to the ActionBarToggle
        mDrawer.setDrawerListener(drawerToggle);

        if (intent != null && transaction_id != null){
            goToRefundFragment(transaction_id);
        }else {
            startPurchase();
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.navigation_drawer_open,  R.string.navigation_drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.nav_purchase:
                fragmentClass = PurchaseFragment.class;
                // Set action bar title
                setTitle("Purchase");
                break;
            case R.id.nav_check_balance:
                fragmentClass = CheckBalanceFragment.class;
                setTitle("Check Balance");
                break;
            case R.id.nav_transaction_history:
                fragmentClass = null;
                startActivity(new Intent(MainActivity.this, TransactionHistoryActivity.class));
                break;
            case R.id.nav_refund:
                fragmentClass = null;
                startActivity(new Intent(MainActivity.this, ApprovedHistoryActivity.class));
                break;
            case R.id.nav_cash_out:
                fragmentClass = CashoutFragment.class;
                setTitle("Cash Out");
                break;
            case R.id.nav_filter:
                fragmentClass = FilterFragment.class;
                setTitle("Filter");
                break;
            case R.id.nav_log_out:
                fragmentClass = null;
                deleteDatabase(DatabaseHelper.DATABASE_NAME);
                Intent intent = new Intent(context, SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            default:
                fragmentClass = PurchaseFragment.class;
                setTitle("Purchase");

        }

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).commit();
        }

        // Highlight the selected item has been done by NavigationView
        // update highlighted item in the navigation menu
        menuItem.setChecked(true);


        // Close the navigation drawer
        mDrawer.closeDrawers();
//        mDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        // disable going back to the SplashActivity or SignUpActivity
        moveTaskToBack(true);
    }

    public void startPurchase(){
        Fragment fragment = null;
        Class fragmentClass;
        fragmentClass = PurchaseFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            setTitle("Purchase");
        } catch (Exception e) {
            e.printStackTrace();
        }
        replaceFragment(fragment);
    }

    public void goToRefundFragment(String transaction_id){
        Bundle bundle = new Bundle();
        bundle.putString("transaction_id", transaction_id);
        Fragment fragment = null;
        Class fragmentClass;
        fragmentClass = RefundFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(bundle);
            setTitle("Refund");
        } catch (Exception e) {
            e.printStackTrace();
        }
        replaceFragment(fragment);
    }



    public void replaceFragment(Fragment someFragment) {
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.mainContent, someFragment).commit();
    }

}
