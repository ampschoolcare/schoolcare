package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.adapters.HistoryListAdapter;
import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.responses.OrderResponse;
import com.amplifypay.schoolcare.methods.HistoryListItem;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.responses.TransactionsResponse;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionHistoryActivity extends AppCompatActivity {

    APIService service;
    Merchant merchant;
    RecyclerView transHistoryRecyclerView;
    HistoryListAdapter historyListAdapter;
    private List<HistoryListItem> historyListItemList = new ArrayList<>();
    TextView merchantDataView;
    Context context;
    Boolean isConnected;
    ImageView backArrow;

    private int pageNumber = 1;
    private int visibleThreshold = 1;
    private int previousTotal = 0;
    private boolean loading = true;
    private int lastVisibleItem, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        this.context = getApplicationContext();
        merchant = Merchant.getThisMerchant();
        service = RestClient.getClient();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews();

        getTransactionHistory();
    }
    private void getTransactionHistory(){
        getOrderResponseFromDB();
        getTransactionHistoryFromServer(1);
    }

    private void setUpViews(){
        backArrow =(ImageView)findViewById(R.id.back_arrow);
        merchantDataView =(TextView)findViewById(R.id.merchant_details);
        if (merchant != null){
            merchantDataView.setText(merchant.fullname);
        }
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        transHistoryRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        historyListAdapter = new HistoryListAdapter(TransactionHistoryActivity.this, historyListItemList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TransactionHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
        transHistoryRecyclerView.setLayoutManager(linearLayoutManager);

        transHistoryRecyclerView.setAdapter(historyListAdapter);


        transHistoryRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        pageNumber++;
                    }
                }


                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    findViewById(R.id.loadMore).setVisibility(View.VISIBLE);
                    getTransactionHistoryFromServer(pageNumber);
                    loading = true;
                }
            }
        });
    }

    private void getTransactionHistoryFromServer(final int pageNumber) {
        final ProgressBar loadingPanel = (ProgressBar) findViewById(R.id.loadingPanel);
        final TextView noTransactionsText = (TextView) findViewById(R.id.no_like_text);

        if (isConnected) {
            if (merchant != null) {
                Call<TransactionsResponse> call = service.getTransactions(merchant.uniqueid, merchant.school_id, String.valueOf(pageNumber));
                call.enqueue(new Callback<TransactionsResponse>() {
                    @Override
                    public void onResponse(Call<TransactionsResponse> call, Response<TransactionsResponse> response) {
                        if (response.isSuccessful()) {
                            loadingPanel.setVisibility(View.GONE);
                            findViewById(R.id.loadMore).setVisibility(View.GONE);


                            // request successful (status code 200, 201)
                            TransactionsResponse transactionsResponse = response.body();

                            List<OrderResponse> orderResponses = transactionsResponse.getTransactions();
                            int size = orderResponses.size();
                            if (size == 0 && pageNumber == 1) {
                                noTransactionsText.setVisibility(View.VISIBLE);
                            }
                            else {
                                noTransactionsText.setVisibility(View.GONE);
                                for (OrderResponse orderResponse : orderResponses) {
                                    if (pageNumber == 1){
                                        orderResponse.save();
                                    }
                                    historyListItemList.add(new HistoryListItem(orderResponse.order_status, orderResponse.datecreated, orderResponse.order_description, orderResponse.amount, String.valueOf(orderResponse.id), orderResponse.student_name, orderResponse.student_gender, orderResponse.school_id, orderResponse.account_number));
                                }
                            }
                            historyListAdapter.notifyDataSetChanged();

                        } else {
                            //request not successful (like 400,401,403 etc)
                            //Handle errors
                            APIError error = LoggingInterceptor.getLastApiError();
                            if (error != null) {
                                // … log the issue :)
                                Log.e("error message", error.msg());
                                // … and use it to show error information
                                Toast.makeText(TransactionHistoryActivity.this, error.msg(), Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<TransactionsResponse> call, Throwable t) {
                        Log.e("response", "failed");
                        Log.e("response", t + "");

                    }
                });
            }
        } else {
            Toast.makeText(this.context, "No internet Connection, Try again", Toast.LENGTH_LONG).show();
            loadingPanel.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void getOrderResponseFromDB() {
        findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
        try {
            Dao<OrderResponse, Integer> orderResponseIntegerDao = AppController.getDatabaseHelper(AppController.getInstance()).getOrderResponseDao();

            QueryBuilder<OrderResponse, Integer> orderResponseIntegerQueryBuilder = orderResponseIntegerDao.queryBuilder().orderBy("id", false);
            List<OrderResponse> orderResponseList = orderResponseIntegerQueryBuilder.query();
            if (!orderResponseList.isEmpty()) {
                for (OrderResponse orderResponse : orderResponseList) {
                    findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                    historyListItemList.add(new HistoryListItem(orderResponse.order_status, orderResponse.datecreated, orderResponse.order_description, orderResponse.amount, String.valueOf(orderResponse.id), orderResponse.student_name, orderResponse.student_gender, orderResponse.school_id, orderResponse.account_number));
                }

                historyListAdapter.notifyDataSetChanged();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
