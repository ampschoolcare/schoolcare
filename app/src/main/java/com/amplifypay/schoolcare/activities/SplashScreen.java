package com.amplifypay.schoolcare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.app.model.School;
import com.amplifypay.schoolcare.services.SchoolService;

import java.sql.SQLException;
import java.util.List;

public class SplashScreen extends AppCompatActivity {
    Context context;
    Merchant merchant;

    List<School> schoolList;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private Button btnGetStarted;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        this.context = getApplicationContext();

        merchant = Merchant.getThisMerchant();

        context.startService(new Intent(context, SchoolService.class));

        try {
            schoolList = AppController.getDatabaseHelper(SplashScreen.this).getSchoolDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        btnGetStarted = (Button) findViewById(R.id.btn);

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start();
            }
        });

        new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        if (merchant != null) {
                            Intent i = new Intent(SplashScreen.this, MainActivity.class);
                            startActivity(i);
                        } else if (!schoolList.isEmpty()) {
                            Intent i = new Intent(SplashScreen.this, SignUpActivity.class);
                            startActivity(i);
                        }

                        // close this activity
                        finish();
                    }
                }, SPLASH_TIME_OUT);
        }


    private void start(){
        if (merchant != null) {
            Intent i = new Intent(SplashScreen.this, MainActivity.class);
            startActivity(i);
        } else if (!schoolList.isEmpty()) {
            Intent i = new Intent(SplashScreen.this, SignUpActivity.class);
            startActivity(i);
        }
    }


}
