package com.amplifypay.schoolcare.rest.responses;

import com.amplifypay.schoolcare.app.model.School;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eit on 1/27/16.
 */
public class SchoolsResponse {
    @SerializedName("schools")
    @Expose
    private List<School> schools = new ArrayList<School>();

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }

    public SchoolsResponse(){
        schools = new ArrayList<School>();
    }

    public static SchoolsResponse parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        SchoolsResponse schoolsResponse = gson.fromJson(response, SchoolsResponse.class);
        return schoolsResponse;
    }
}
