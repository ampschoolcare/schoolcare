package com.amplifypay.schoolcare.rest.responses;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eit on 1/27/16.
 */
public class TransactionsResponse {
    @SerializedName("transactions")
    @Expose
    private List<OrderResponse> transactions = new ArrayList<OrderResponse>();

    public List<OrderResponse> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<OrderResponse> transactions) {
        this.transactions = transactions;
    }

    public TransactionsResponse(){
        transactions = new ArrayList<OrderResponse>();
    }

    public static TransactionsResponse parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        TransactionsResponse transactionsResponse = gson.fromJson(response, TransactionsResponse.class);
        return transactionsResponse;
    }
}
