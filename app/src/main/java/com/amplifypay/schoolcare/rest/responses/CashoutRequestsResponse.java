package com.amplifypay.schoolcare.rest.responses;

import com.amplifypay.schoolcare.app.model.Cashout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eit on 1/27/16.
 */
public class CashoutRequestsResponse {
    @SerializedName("cashout_requests")
    @Expose
    private List<Cashout> cashout_requests = new ArrayList<Cashout>();

    public List<Cashout> getCashout_requests() {
        return cashout_requests;
    }

    public void setCashout_requests(List<Cashout> cashout_requests) {
        this.cashout_requests = cashout_requests;
    }

    public CashoutRequestsResponse(){
        cashout_requests = new ArrayList<Cashout>();
    }

    public static CashoutRequestsResponse parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        CashoutRequestsResponse cashoutRequestsResponse = gson.fromJson(response, CashoutRequestsResponse.class);
        return cashoutRequestsResponse;
    }
}
