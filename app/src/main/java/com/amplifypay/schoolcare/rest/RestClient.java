package com.amplifypay.schoolcare.rest;

import com.amplifypay.schoolcare.rest.services.APIService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by eit on 1/27/16.
 */
public class RestClient {

    private static String baseUrl = "http://api.schoolcare.com.ng/index.php/";


    private static APIService apiService;


    public interface OnConnectionTimeoutListener {
        void onConnectionTimeout();
    }

    public static APIService getClient() {
        if (apiService == null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            
            // add your other interceptors …
            httpClient.connectTimeout(10, TimeUnit.SECONDS);
            httpClient.readTimeout(30, TimeUnit.SECONDS);
            httpClient.retryOnConnectionFailure(true);

            // add logging as last interceptor
            httpClient.interceptors().add(new LoggingInterceptor());
            httpClient.interceptors().add(logging);  // <-- this is the important line!


            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            
            apiService = client.create(APIService.class);
        }
        return apiService;
    }

}
