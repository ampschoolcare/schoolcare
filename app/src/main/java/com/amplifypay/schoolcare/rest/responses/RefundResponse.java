package com.amplifypay.schoolcare.rest.responses;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class RefundResponse {
    public static final String TAG = "Refund Response";

    @DatabaseField(id = true)
    public int transaction_id;

    @DatabaseField
    public String agent_id;

    @DatabaseField
    public String school_id;

    @DatabaseField
    public String reason;

    @DatabaseField
    public String datecreated;

    @DatabaseField
    public String datemodified;



    // Default constructor is needed for the SQLite, so make sure you also have it
    public RefundResponse(){

    }

    public RefundResponse(int transaction_id, String agent_id, String school_id, String reason, String datecreated, String datemodified){
        this.transaction_id = transaction_id;
        this.agent_id = agent_id;
        this.school_id = school_id;
        this.reason = reason;
        this.datecreated = datecreated;
        this.datemodified = datemodified;

    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getRefundResponseDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }
}
