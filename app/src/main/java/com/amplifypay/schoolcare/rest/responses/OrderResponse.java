package com.amplifypay.schoolcare.rest.responses;

import android.util.Log;

import com.amplifypay.schoolcare.app.AppController;
import com.amplifypay.schoolcare.app.DatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by eit on 11/17/15.
 */
@DatabaseTable
public class OrderResponse {
    public static final String TAG = "Order Response";

    @DatabaseField(id = true)
    public int id;

    @DatabaseField
    public String agent_id;

    @DatabaseField
    public String school_id;

    @DatabaseField
    public String account_number;

    @DatabaseField
    public String order_status;

    @DatabaseField
    public String order_description;

    @DatabaseField
    public String response_description;

    @DatabaseField
    public String amount;

    @DatabaseField
    public String student_name;

    @DatabaseField
    public String student_gender;

    @DatabaseField
    public String datecreated;

    @DatabaseField
    public String datemodified;



    // Default constructor is needed for the SQLite, so make sure you also have it
    public OrderResponse(){

    }

    public OrderResponse(int id, String agent_id, String school_id, String account_number, String order_status, String order_description, String response_description, String amount, String student_name, String student_gender, String datecreated, String datemodified){
        this.id = id;
        this.agent_id = agent_id;
        this.school_id = school_id;
        this.account_number = account_number;
        this.order_status = order_status;
        this.order_description = order_description;
        this.amount = amount;
        this.student_name = student_name;
        this.student_gender = student_gender;
        this.datecreated = datecreated;
        this.datemodified = datemodified;
        this.response_description = response_description;

    }

    public boolean save(){
        try {
            DatabaseHelper dbHelper = AppController.getDatabaseHelper(AppController.getInstance());
            Dao.CreateOrUpdateStatus status = dbHelper.getOrderResponseDao().createOrUpdate(this);
            dbHelper.close();
            return (status.isCreated() || status.isUpdated());
        } catch (Exception exp) {
            exp.printStackTrace();
            Log.e(TAG, "the error in save --->" + exp.getMessage());
            return false;
        }
    }
}
