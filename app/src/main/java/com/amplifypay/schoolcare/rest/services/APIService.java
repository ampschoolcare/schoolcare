package com.amplifypay.schoolcare.rest.services;

import com.amplifypay.schoolcare.app.model.Balance;
import com.amplifypay.schoolcare.app.model.Cashout;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.responses.CashoutRequestsResponse;
import com.amplifypay.schoolcare.rest.responses.OrderResponse;
import com.amplifypay.schoolcare.rest.responses.RefundResponse;
import com.amplifypay.schoolcare.rest.responses.SchoolsResponse;
import com.amplifypay.schoolcare.rest.responses.TransactionsResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by eit on 1/27/16.
 */
public interface APIService {

    // get_schools
    @POST("api/schools")
    Call<SchoolsResponse> getSchools(@Body JsonObject clientUser);

    // sign_in merchant
    @POST("api/agents")
    Call<Merchant> signInMerchant(@Body JsonObject merchantObject);

    //confirm_order
    @POST("api/transactions/confirmorder")
    Call<OrderResponse> confirmOrder(@Body JsonObject orderObject);

    //make_purchase
    @POST("api/transactions/purchase")
    Call<OrderResponse> makePurchase(@Body JsonObject orderObject);

    // transaction history
    @GET("api/transactions/{agentId}/{schoolId}/{pageNumber}")
    Call<TransactionsResponse> getTransactions(@Path("agentId") String agentId, @Path("schoolId") String schoolId, @Path("pageNumber") String pageNumber);

    //refund
    @POST("api/transactions/requestrefund")
    Call<RefundResponse> requestRefund(@Body JsonObject orderObject);

    //balance
    @POST("api/transactions/balance")
    Call<Balance> checkBalance(@Body JsonObject orderObject);

    // transaction history approved - only
    @GET("api/transactions/{agentId}/{schoolId}/{pageNumber}/Approved")
    Call<TransactionsResponse> getApprovedTransactions(@Path("agentId") String agentId, @Path("schoolId") String schoolId, @Path("pageNumber") String pageNumber);

    //request cashout
    @POST("api/cashouts/request")
    Call<Cashout> requestCashOut(@Body JsonObject requestObject);

    // get cashout history
    @GET("api/cashouts/{agentId}/{schoolId}/{pageNumber}")
    Call<CashoutRequestsResponse> getCashOutHistory(@Path("agentId") String agentId, @Path("schoolId") String schoolId, @Path("pageNumber") String pageNumber);

    //cancel cashout request
    @POST("api/cashouts/cancel")
    Call<Cashout> canceCashoutRequest(@Body JsonObject requestObject);
}


