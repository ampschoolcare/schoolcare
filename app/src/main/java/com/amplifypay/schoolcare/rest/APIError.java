package com.amplifypay.schoolcare.rest;

/**
 * Created by eit on 1/27/16.
 */
public class APIError {
    private int statusCode;
    private String msg;
    private String error;

    public APIError() {
    }

    public APIError(int statusCode, String msg, String error){
        this.statusCode = statusCode;
        this.msg = msg;
        this.error = error;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String msg() {
        return msg;
    }

    public String error() {
        return error;
    }
}
