package com.amplifypay.schoolcare.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.amplifypay.schoolcare.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by eit on 8/2/16.
 */
public class AppUtil {
    private static String TAG = AppUtil.class.getSimpleName();

    public static void hideKeyboard(Activity activity, View viewToHide) {
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewToHide.getWindowToken(), 0);
    }

    public static void showTransactionResponse(Context context, boolean success, String desc, final Callback callback) {
        final Dialog dialog = new Dialog(context); // Context, this, etc.
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.payment_result, null);
        TextView text = (TextView) v.findViewById(R.id.textView);
        ImageView img = (ImageView) v.findViewById(R.id.imageView);
        TextView description = (TextView) v.findViewById(R.id.response_description);
        TextView ok = (TextView) v.findViewById(R.id.response_ok);

        String msg;
        int drawable;

        if (success) {
            msg = "Payment Successful";
            drawable = R.drawable.success;
        } else {
            msg = "Payment Failed";
            drawable = R.drawable.fail;
        }

        if (!TextUtils.isEmpty(desc)) {
            description.setVisibility(View.VISIBLE);
            description.setText(desc);
        }

        text.setText(msg);
        img.setImageResource(drawable);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ok.setVisibility(View.VISIBLE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(v);
//        dialog.setCancelable(false);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                callback.callback();
            }
        });
        dialog.show();
    }


    public static void showBalanceResponse(Context context, String student_data,  String acct_bal, final Callback callback) {
        final Dialog dialog = new Dialog(context); // Context, this, etc.
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.balance_result, null);


        TextView accountBalView = (TextView) v.findViewById(R.id.acc_bal_amount);
        TextView studentDataView = (TextView) v.findViewById(R.id.student_data);


        TextView ok = (TextView) v.findViewById(R.id.response_ok);

        accountBalView.setText(AppUtil.currencyNaira(acct_bal));
        studentDataView.setText(student_data);

        ok.setVisibility(View.VISIBLE);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(v);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                callback.callback();
            }
        });
        dialog.show();
    }




    public interface Callback {
        void callback();
    }

    public static String dateReadable(Context context, String date){
        String justDateTime = date.split(Pattern.quote("."))[0];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date testDate = null;
        try {
            testDate = sdf.parse(justDateTime);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        assert testDate != null;
        Long timeInMills = testDate.getTime();
        return DateUtils.formatDateTime(context, timeInMills, DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_SHOW_DATE);
    }

    public static String currencyNaira(String amount){
        return "₦" + String.format("%,d", Integer.valueOf(amount));
    }
}
