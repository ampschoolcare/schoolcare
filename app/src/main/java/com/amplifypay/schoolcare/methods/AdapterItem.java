package com.amplifypay.schoolcare.methods;

/**
 * Created by eit on 7/24/16.
 */
public class AdapterItem {
    private String id;
    private String name;


    public AdapterItem() {

    }
    public AdapterItem(String id, String name) {
        this.id = id;
        this.name = name;


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
