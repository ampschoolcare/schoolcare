package com.amplifypay.schoolcare.methods;

/**
 * Created by eit on 4/7/16.
 */
public class CashoutListItem {

    public String request_id;
//    public String uniqueid;
//    public String merchant_uniqueid;
//    public String school_id;
    public String cashout_amount;
    public String additional_info;
    public String request_date;
    public String request_disburse_date;
    public String request_status;
    public String request_handled_by;
    public String request_date_handled;



    public CashoutListItem(String request_id, String request_date, String cashout_amount, String additional_info, String request_status, String request_disburse_date, String request_handled_by, String request_date_handled) {
        this.request_id = request_id;
        this.request_date = request_date;
        this.cashout_amount = cashout_amount;
        this.additional_info = additional_info;
        this.request_status = request_status;
        this.request_disburse_date = request_disburse_date;
        this.request_handled_by = request_handled_by;
        this.request_date_handled = request_date_handled;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getCashout_amount() {
        return cashout_amount;
    }

    public void setCashout_amount(String cashout_amount) {
        this.cashout_amount = cashout_amount;
    }

    public String getAdditional_info() {
        return additional_info;
    }

    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getRequest_disburse_date() {
        return request_disburse_date;
    }

    public void setRequest_disburse_date(String request_disburse_date) {
        this.request_disburse_date = request_disburse_date;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getRequest_handled_by() {
        return request_handled_by;
    }

    public void setRequest_handled_by(String request_handled_by) {
        this.request_handled_by = request_handled_by;
    }

    public String getRequest_date_handled() {
        return request_date_handled;
    }

    public void setRequest_date_handled(String request_date_handled) {
        this.request_date_handled = request_date_handled;
    }
}
