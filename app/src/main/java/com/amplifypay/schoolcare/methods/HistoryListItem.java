package com.amplifypay.schoolcare.methods;

/**
 * Created by eit on 4/7/16.
 */
public class HistoryListItem {
    private String date;
    private String order_description;
    private String order_amount;
    private String transaction_id;
    private String student_name;
    private String student_gender;
    private String school_id;
    private String order_status;
    private String account_number;



    public HistoryListItem(String order_status, String date, String order_description, String order_amount, String transaction_id, String student_name, String student_gender, String school_id, String account_number) {
        this.transaction_id = transaction_id;
        this.date = date;
        this.order_amount = order_amount;
        this.order_description = order_description;
        this.student_name = student_name;
        this.student_gender = student_gender;
        this.school_id = school_id;
        this.order_status = order_status;
        this.account_number = account_number;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrder_description() {
        return order_description;
    }

    public void setOrder_description(String order_description) {
        this.order_description = order_description;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_gender() {
        return student_gender;
    }

    public void setStudent_gender(String student_gender) {
        this.student_gender = student_gender;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }
}
