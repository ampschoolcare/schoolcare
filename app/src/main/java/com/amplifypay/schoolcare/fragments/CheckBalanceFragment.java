package com.amplifypay.schoolcare.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.model.Balance;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.amplifypay.schoolcare.utils.AppUtil;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.gson.JsonObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eit on 7/24/16.
 */
public class CheckBalanceFragment extends Fragment {

    private EditText inputCardNumber;
    private TextInputLayout inputCardNumberLayout;
    private AppCompatButton btnProceed;
    private String card_number;
    APIService service;
    Context context;
    Merchant merchant;
    Boolean isConnected;
    private RelativeLayout cardOption, qrCodeOption, balanceOptionsLayout;
    private CheckBox qrCodeCheck, cardCheck;
    TextView studentDataView, accountBalView, accountBalLabel;
    SurfaceView cameraView;
    TextView barcodeInfo;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;


    public CheckBalanceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_check_balance, container, false);
        this.context = getActivity().getApplicationContext();

        service = RestClient.getClient();
        merchant = Merchant.getThisMerchant();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews(rootView);



        // Inflate the layout for this fragment
        return rootView;
    }

    private void setUpViews(View rootView){

        inputCardNumberLayout = (TextInputLayout) rootView.findViewById(R.id.input_card_number_layout);
        inputCardNumber = (EditText) rootView.findViewById(R.id.input_card_number);

        cardOption = (RelativeLayout) rootView.findViewById(R.id.card_option_layout);
        qrCodeOption = (RelativeLayout) rootView.findViewById(R.id.qr_option_layout);

        balanceOptionsLayout = (RelativeLayout) rootView.findViewById(R.id.balance_options_layout);

        qrCodeCheck = (CheckBox) rootView.findViewById(R.id.checkboxQR);
        cardCheck = (CheckBox) rootView.findViewById(R.id.checkboxCard);

        cameraView = (SurfaceView) rootView.findViewById(R.id.camera_view);
        barcodeInfo = (TextView) rootView.findViewById(R.id.code_info);

        addListenerOnQRCheck();
        addListenerOnCardCheck();

        btnProceed = (AppCompatButton) rootView.findViewById(R.id.btn_proceed);

        inputCardNumber.addTextChangedListener(new MyTextWatcher(inputCardNumber));

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });



    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_card_number:
                    validateCardNumber();
                    break;
            }
        }
    }


    private boolean validateCardNumber() {
        if (inputCardNumber.getText().toString().trim().isEmpty()) {
            inputCardNumberLayout.setError("Please enter your 10 digits card number");
            requestFocus(inputCardNumber);
            return false;
        } else if (inputCardNumber.getText().toString().trim().length() < 10){
            inputCardNumberLayout.setError("Your card number cannot be less than 10 digits");
            requestFocus(inputCardNumber);
            return false;
        } else if (inputCardNumber.getText().toString().trim().length() > 10){
            inputCardNumberLayout.setError("Your card number cannot be more than 10 digits");
            requestFocus(inputCardNumber);
            return false;
        }else {
            inputCardNumberLayout.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    private void submitForm() {
        if (barcodeInfo.getVisibility() == View.VISIBLE) {
            if (!validateQRCodeCardNumber()) {
                return;
            }
            card_number = barcodeInfo.getText().toString().trim();
            checkBalance(card_number);
        }else if (inputCardNumberLayout.getVisibility() == View.VISIBLE){
            if (!validateCardNumber()) {
                return;
            }
            card_number = inputCardNumber.getText().toString().trim();
            checkBalance(card_number);
        }
    }

    private boolean validateQRCodeCardNumber() {
        if (barcodeInfo.getText().toString().trim().isEmpty()) {
            barcodeInfo.setError("Scan again, your QR Code must return a 10 digits number");
            return false;
        } else if (barcodeInfo.getText().toString().trim().length() < 10) {
            barcodeInfo.setError("Scan again, your QR Code number cannot be less than 10 digits");
            return false;
        } else if (barcodeInfo.getText().toString().trim().length() > 10) {
            barcodeInfo.setError("Scan again, your QR Code number cannot be more than 10 digits");
            return false;
        }
        return true;
    }

    private void checkBalance(String card_number){
        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("account_number", card_number);
        }
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Checking...");
        pDialog.show();

        Call<Balance> call = service.checkBalance(jsonObject);
        call.enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(Call<Balance> call, Response<Balance> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Balance balance = response.body();
                    Toast.makeText(context, "Balance request successful", Toast.LENGTH_SHORT).show();
                    showBalance(balance);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<Balance> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });

    }

    private void showBalance(Balance balance){
        if (balance != null) {
            AppUtil.showBalanceResponse(getActivity(), balance.student_name + " - " + balance.student_gender, balance.account_balance, new AppUtil.Callback() {
                @Override
                public void callback() {
                    removeFragment();
                }
            });
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void addListenerOnQRCheck() {

        qrCodeCheck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is qr code checked?
                if (((CheckBox) v).isChecked()) {
                    balanceOptionsLayout.setVisibility(View.GONE);
                    inputCardNumberLayout.setVisibility(View.GONE);
                    cameraView.setVisibility(View.VISIBLE);
                    barcodeInfo.setVisibility(View.VISIBLE);
                    btnProceed.setVisibility(View.VISIBLE);
                    setupQRCodeFunctions();
                }
            }
        });

    }

    public void addListenerOnCardCheck() {

        cardCheck.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is card checked?
                if (((CheckBox) v).isChecked()) {
                    balanceOptionsLayout.setVisibility(View.GONE);
                    cameraView.setVisibility(View.GONE);
                    barcodeInfo.setVisibility(View.GONE);
                    inputCardNumberLayout.setVisibility(View.VISIBLE);
                    btnProceed.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setupQRCodeFunctions(){
        barcodeDetector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource
                .Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(320, 400)
                .build();


        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    barcodeInfo.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            barcodeInfo.setText(    // Update the TextView
                                    barcodes.valueAt(0).displayValue
                            );
                        }
                    });
                }
            }
        });
    }

    public void removeFragment() {
        Fragment fragment = null;
        Class fragmentClass = CheckBalanceFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainContent, fragment)
                .commit();
    }


}
