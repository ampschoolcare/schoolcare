package com.amplifypay.schoolcare.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.activities.CardPaymentActivity;
import com.amplifypay.schoolcare.activities.QRCodePaymentActivity;

/**
 * Created by eit on 7/24/16.
 */
public class PurchaseFragment extends Fragment {
    Context context;
    Boolean isConnected;

    private EditText inputAmount, inputDescription;
    private TextInputLayout inputAmountLayout, inputDescriptionLayout;
    private AppCompatButton btnProceed;
    private RelativeLayout cardOption, qrCodeOption;
    private CheckBox qrCodeCheck, cardCheck;
    private TextView paymentOptionText;
    String trans_amount, trans_description;


    public PurchaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_purchase, container, false);

        this.context = getActivity().getApplicationContext();


        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews(rootView);

        // Inflate the layout for this fragment
        return rootView;
    }

    private void setUpViews(View rootView){

        paymentOptionText = (TextView) rootView.findViewById(R.id.payment_option);

        inputAmountLayout = (TextInputLayout) rootView.findViewById(R.id.input_amount_layout);
        inputAmount = (EditText) rootView.findViewById(R.id.input_amount);

        inputDescriptionLayout = (TextInputLayout) rootView.findViewById(R.id.input_desc_layout);
        inputDescription = (EditText) rootView.findViewById(R.id.input_description);

        cardOption = (RelativeLayout) rootView.findViewById(R.id.card_option_layout);
        qrCodeOption = (RelativeLayout) rootView.findViewById(R.id.qr_option_layout);

        qrCodeCheck = (CheckBox) rootView.findViewById(R.id.checkboxQR);
        cardCheck = (CheckBox) rootView.findViewById(R.id.checkboxCard);

        btnProceed = (AppCompatButton) rootView.findViewById(R.id.btn_proceed);

        inputAmount.addTextChangedListener(new MyTextWatcher(inputAmount));
        inputDescription.addTextChangedListener(new MyTextWatcher(inputDescription));

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });



    }

    private void submitForm() {
        //validate Amount & Description
        //validate that atleast 1 payment option is checked - can't check multiple payment option

        trans_amount = inputAmount.getText().toString().trim();
        trans_description = inputDescription.getText().toString().trim();

        if (!validateAmount()) {
            return;
        }

        if (!validateDescription()) {
            return;
        }
        if (!validatePaymentOptionChecked()) {
            return;
        }

        // proceed
        //depending on option checked
        if (cardCheck.isChecked()){
            goToCardPayment(trans_amount, trans_description);
        }else if (qrCodeCheck.isChecked()){
            goToQRCodePayment(trans_amount, trans_description);
        }

    }

    private boolean validateAmount() {
        if (inputAmount.getText().toString().trim().isEmpty()) {
            inputAmountLayout.setError("Please enter an Amount");
            requestFocus(inputAmount);
            return false;
        }else if (Integer.valueOf(inputAmount.getText().toString().trim()) < 1) {
            inputAmountLayout.setError("Amount can't be 0");
            requestFocus(inputAmount);
            return false;
        }else {
            inputAmountLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateDescription() {
        if (inputDescription.getText().toString().trim().isEmpty()) {
            inputDescriptionLayout.setError("Please enter a Description");
            requestFocus(inputDescription);
            return false;
        }else {
            inputDescriptionLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePaymentOptionChecked() {
        if (!cardCheck.isChecked() && !qrCodeCheck.isChecked()) {
            Toast.makeText(context, "Please select a Payment Option", Toast.LENGTH_SHORT).show();
            requestFocus(cardCheck);
            requestFocus(qrCodeCheck);
            return false;
        }
        else if (cardCheck.isChecked() && qrCodeCheck.isChecked()) {
            Toast.makeText(context, "You can only select one Payment Option", Toast.LENGTH_SHORT).show();
            requestFocus(cardCheck);
            requestFocus(qrCodeCheck);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_amount:
                    validateAmount();
                    break;
                case R.id.input_description:
                    validateDescription();
                    break;
            }
        }
    }


    private void goToCardPayment(String trans_amount, String trans_description){
        Intent i = new Intent(context, CardPaymentActivity.class);
        String formatted_amount = String.format("%,d", Integer.valueOf(trans_amount));
        i.putExtra("formatted_amount", formatted_amount);
        i.putExtra("amount", trans_amount);
        i.putExtra("description", trans_description);
        startActivity(i);
    }

    private void goToQRCodePayment(String trans_amount, String trans_description){
        Intent i = new Intent(context, QRCodePaymentActivity.class);
        String formatted_amount = String.format("%,d", Integer.valueOf(trans_amount));
        i.putExtra("formatted_amount", formatted_amount);
        i.putExtra("amount", trans_amount);
        i.putExtra("description", trans_description);
        startActivity(i);
    }

}
