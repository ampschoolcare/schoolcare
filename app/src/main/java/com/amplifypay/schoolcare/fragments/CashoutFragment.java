package com.amplifypay.schoolcare.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.activities.CashoutDetailActivity;
import com.amplifypay.schoolcare.activities.CashoutHistoryActivity;
import com.amplifypay.schoolcare.app.model.Cashout;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eit on 7/24/16.
 */
public class CashoutFragment extends Fragment {
    private EditText inputAmount, inputDesc;
    private TextInputLayout inputAmountLayout, inputDescLayout;
    private AppCompatButton btnProceed,btnShowCashoutHistory, btnViewCashoutDetails;
    private LinearLayout cashoutRequestLayout, cashoutStatusLayout;
    private String amount, desc, request_id, request_date, formatted_amount, cashout_amount, additional_info, request_status, request_disburse_date, request_handled_by, request_date_handled;
    APIService service;
    Context context;
    Merchant merchant;
    Boolean isConnected;
    TextView request_date_text, cashout_amount_text, additional_info_text, request_status_text, request_disburse_date_text, request_handled_by_text, request_date_handled_text;


    public CashoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        transaction_id = getArguments().getString("transaction_id");
        View rootView = inflater.inflate(R.layout.fragment_cashout, container, false);

        this.context = getActivity().getApplicationContext();

        service = RestClient.getClient();
        merchant = Merchant.getThisMerchant();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews(rootView);


        // Inflate the layout for this fragment
        return rootView;
    }

    private void setUpViews(View rootView){
        cashoutRequestLayout = (LinearLayout) rootView.findViewById(R.id.cashout_request_layout);

        cashoutStatusLayout = (LinearLayout) rootView.findViewById(R.id.cashout_status_layout);

        inputAmountLayout = (TextInputLayout) rootView.findViewById(R.id.input_amount_layout);
        inputAmount = (EditText) rootView.findViewById(R.id.input_amount);

        inputDescLayout = (TextInputLayout) rootView.findViewById(R.id.input_reason_layout);
        inputDesc = (EditText) rootView.findViewById(R.id.input_reason);

        btnProceed = (AppCompatButton) rootView.findViewById(R.id.btn_proceed);

        btnShowCashoutHistory = (AppCompatButton) rootView.findViewById(R.id.btn_cash_out_history);

        inputAmount.addTextChangedListener(new MyTextWatcher(inputAmount));
        inputDesc.addTextChangedListener(new MyTextWatcher(inputDesc));

        btnViewCashoutDetails = (AppCompatButton) rootView.findViewById(R.id.btn_view_details);

        request_date_text = (TextView) rootView.findViewById(R.id.request_date_text);

        request_disburse_date_text = (TextView) rootView.findViewById(R.id.disburse_date_text);

        request_date_handled_text = (TextView) rootView.findViewById(R.id.date_handled);

        cashout_amount_text = (TextView) rootView.findViewById(R.id.amount_text);

        additional_info_text = (TextView) rootView.findViewById(R.id.description);

        request_handled_by_text = (TextView) rootView.findViewById(R.id.request_handled_by);

        request_status_text = (TextView) rootView.findViewById(R.id.cashout_status);


        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        btnShowCashoutHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCashOutHistory();
            }
        });



    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_reason:
                    validateReason();
                    break;
                case R.id.input_amount:
                    validateAmount();
                    break;
            }
        }
    }


    private boolean validateReason() {
        if (inputDesc.getText().toString().trim().isEmpty()) {
            inputDescLayout.setError("Please enter additional information for cash out");
            requestFocus(inputDesc);
            return false;
        } else {
            inputDescLayout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAmount() {
        //check amount
        if (inputAmount.getText().toString().trim().isEmpty()) {
            inputAmountLayout.setError("Please enter an amount");
            requestFocus(inputAmount);
            return false;
        }else if (Integer.valueOf(inputAmount.getText().toString().trim()) < 1) {
            inputAmountLayout.setError("Amount can't be 0");
            requestFocus(inputAmount);
            return false;
        } else {
            inputAmountLayout.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    private void submitForm() {
        amount = inputAmount.getText().toString().trim();
        desc = inputDesc.getText().toString().trim();

        if (!validateReason()) {
            return;
        }

        if (!validateAmount()) {
            return;
        }
        makeCashOutRequest();

    }

    private void makeCashOutRequest(){
        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("amount", amount);
            jsonObject.addProperty("additional_info", desc);
        }

        Call<Cashout> call = service.requestCashOut(jsonObject);
        call.enqueue(new Callback<Cashout>() {
            @Override
            public void onResponse(Call<Cashout> call, Response<Cashout> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Cashout cashout = response.body();
                    cashout.save();
                    Toast.makeText(context, "Cash out request successful", Toast.LENGTH_SHORT).show();
                    showCashOutStatus(cashout);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<Cashout> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showCashOutStatus(Cashout cashout){
        cashoutRequestLayout.setVisibility(View.GONE);
        btnShowCashoutHistory.setVisibility(View.GONE);
        cashoutStatusLayout.setVisibility(View.VISIBLE);
        Log.e("cashout status", cashout.request_status);

        request_id = String.valueOf(cashout.id);
        request_date = cashout.request_date;
        cashout_amount = cashout.amount;
        additional_info = cashout.additional_info;
        request_status = cashout.request_status;
        request_disburse_date = cashout.request_disburse_date;
        request_handled_by = cashout.request_handled_by;
        request_date_handled = cashout.request_date_handled;
        formatted_amount = String.format("%,d", Integer.valueOf(cashout.amount));

        request_date_text.setText(request_date);

        request_disburse_date_text.setText(request_disburse_date);

        request_date_handled_text.setText(request_date_handled);

        cashout_amount_text.setText("₦"+ formatted_amount);

        additional_info_text.setText(additional_info);

        request_handled_by_text.setText(request_handled_by);

        request_status_text.setText(request_status);

        if (request_status.equalsIgnoreCase("Approved")){
            request_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }else if (request_status.equalsIgnoreCase("Pending")){
            request_status_text.setTextColor(getResources().getColor( R.color.error_color));
        }else {
            request_status_text.setTextColor(getResources().getColor( R.color.primaryColor));
        }

        btnViewCashoutDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCashOutDetails(request_id, request_date, formatted_amount, cashout_amount, additional_info, request_status, request_disburse_date, request_handled_by, request_date_handled);
            }
        });

    }


    private void goToCashOutHistory(){
        Intent i = new Intent(context, CashoutHistoryActivity.class);
        startActivity(i);
    }

    private void viewCashOutDetails(String request_id, String request_date, String formatted_amount, String cashout_amount, String additional_info, String request_status, String request_disburse_date, String request_handled_by, String request_date_handled){
        Intent i = new Intent(context, CashoutDetailActivity.class);
        i.putExtra("request_id", request_id);
        i.putExtra("request_date", request_date);
        i.putExtra("formatted_amount", formatted_amount);
        i.putExtra("cashout_amount", cashout_amount);
        i.putExtra("additional_info", additional_info);
        i.putExtra("request_status", request_status);
        i.putExtra("request_disburse_date", request_disburse_date);
        i.putExtra("request_handled_by", request_handled_by);
        i.putExtra("request_date_handled", request_date_handled);

        startActivity(i);
    }
}
