package com.amplifypay.schoolcare.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.amplifypay.schoolcare.R;
import com.amplifypay.schoolcare.app.model.Merchant;
import com.amplifypay.schoolcare.rest.responses.RefundResponse;
import com.amplifypay.schoolcare.rest.APIError;
import com.amplifypay.schoolcare.rest.LoggingInterceptor;
import com.amplifypay.schoolcare.rest.RestClient;
import com.amplifypay.schoolcare.rest.services.APIService;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by eit on 7/24/16.
 */
public class RefundFragment extends Fragment {
    private EditText inputTransactionID, inputReason;
    private TextInputLayout inputTransactionIDLayout, inputReasonLayout;
    private AppCompatButton btnProceed;
    private String transaction_id, reason;
    APIService service;
    Context context;
    Merchant merchant;
    Boolean isConnected;

    public RefundFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        transaction_id = getArguments().getString("transaction_id");
        View rootView = inflater.inflate(R.layout.fragment_refund, container, false);

        this.context = getActivity().getApplicationContext();

        service = RestClient.getClient();
        merchant = Merchant.getThisMerchant();

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        setUpViews(rootView);


        // Inflate the layout for this fragment
        return rootView;
    }

    private void setUpViews(View rootView){


//        inputTransactionIDLayout = (TextInputLayout) rootView.findViewById(R.id.input_transaction_id_layout);
//        inputTransactionID = (EditText) rootView.findViewById(R.id.input_transaction_id);

        inputReasonLayout = (TextInputLayout) rootView.findViewById(R.id.input_reason_layout);
        inputReason = (EditText) rootView.findViewById(R.id.input_reason);

        btnProceed = (AppCompatButton) rootView.findViewById(R.id.btn_proceed);

//        inputTransactionID.addTextChangedListener(new MyTextWatcher(inputTransactionID));
        inputReason.addTextChangedListener(new MyTextWatcher(inputReason));

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });



    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_reason:
                    validateReason();
                    break;
//                case R.id.input_transaction_id:
//                    validateTransactionID();
//                    break;
            }
        }
    }


    private boolean validateReason() {
        if (inputReason.getText().toString().trim().isEmpty()) {
            inputReasonLayout.setError("Please enter reason for the refund");
            requestFocus(inputReason);
            return false;
        } else {
            inputReasonLayout.setErrorEnabled(false);
        }

        return true;
    }

//    private boolean validateTransactionID() {
//        if (inputTransactionID.getText().toString().trim().isEmpty()) {
//            inputTransactionIDLayout.setError("Please enter a transaction ID");
//            requestFocus(inputTransactionID);
//            return false;
//        } else {
//            inputTransactionIDLayout.setErrorEnabled(false);
//        }
//
//        return true;
//    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    private void submitForm() {
//        transaction_id = inputTransactionID.getText().toString().trim();
        reason = inputReason.getText().toString().trim();

        if (!validateReason()) {
            return;
        }

//        if (!validateTransactionID()) {
//            return;
//        }
        makeRefundRequest();

    }

    private void makeRefundRequest(){
//        card_number = cardField.getText().toString().trim();
        JsonObject jsonObject = new JsonObject();
        if (merchant != null) {
            jsonObject.addProperty("agent_id", merchant.uniqueid);
            jsonObject.addProperty("school_id", merchant.school_id);
            jsonObject.addProperty("transaction_id", transaction_id);
            jsonObject.addProperty("reason", reason);
        }
//        final ProgressDialog pDialog = new ProgressDialog(context);
//        pDialog.setMessage("Requesting...");
//        pDialog.show();

        Call<RefundResponse> call = service.requestRefund(jsonObject);
        call.enqueue(new Callback<RefundResponse>() {
            @Override
            public void onResponse(Call<RefundResponse> call, Response<RefundResponse> response) {
//                pDialog.dismiss();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    RefundResponse refundResponse = response.body();
                    refundResponse.save();
                    Toast.makeText(context, "Refund request successful", Toast.LENGTH_SHORT).show();
//                    completePurchase(orderResponse);
                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    APIError error = LoggingInterceptor.getLastApiError();
                    // … log the issue :)
                    if (error != null) {
                        Log.e("error message", error.msg());
                        Toast.makeText(context, error.msg(), Toast.LENGTH_SHORT).show();

                        // … and use it to show error information
                    }

                }

            }

            @Override
            public void onFailure(Call<RefundResponse> call, Throwable t) {
                Log.e("response", "failed");
                Log.e("response t", t + "");
            }

        });

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
